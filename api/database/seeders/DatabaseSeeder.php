<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()->create([
            'name' => 'Jhon doe teste migration insertion',
            'email' => 'test@example.com',
            'document' => '01453278520',
            'address' => 'LOGO ALI PARA UM TESTE',
            'state' => 'DF',
            'city' => 'BRASILIA',
            'sexology' => 'Male',
            'birthday' => Carbon::now()->subYears(30),
            'active' => 'A'
        ]);
    }
}
