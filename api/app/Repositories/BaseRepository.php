<?php

namespace App\Repositories;

use Illuminate\Contracts\Database\Query\Expression;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;

abstract class BaseRepository
{
    protected Model $model;

    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Create a new record in the database.
     *
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model
    {
        return $this->model->create($data);
    }

    /**
     * Find or create a record in the database.
     *
     * @param array $fetchData
     * @param array $data
     * @return Model
     */
    public function firstOrCreate(array $fetchData, array $data): Model
    {
        return $this->model->firstOrCreate($fetchData, $data);
    }

    /**
     * Create many records in the database.
     *
     * @param array $items
     * @return int
     */
    public function createMany(array $items): int
    {
        $count = 0;

        foreach ($items as $item) {
            $this->create($item);
            $count++;
        }

        return $count;
    }

    /**
     * Update a record in the database.
     *
     * @param mixed $id
     * @param array $data
     * @return bool
     */
    public function update($id, array $data): bool
    {
        $model = $this->model->findOrFail($id);

        $model->fill($data);

        return $model->save();
    }

    /**
     * Update or Create a record in the database.
     *
     * @param array $filters
     * @param array $data
     * @return Model
     */
    public function updateOrCreate(array $filters, array $data): Model
    {
        return $this->model->updateOrCreate($filters, $data);
    }

    /**
     * Update records by filter in the database.
     *
     * @param array $filters
     * @param array $data
     * @return int
     */
    public function updateBy(array $filters, array $data): int
    {
        $query = $this->model->newQuery();

        $this->applyDynamicFilters($query, $filters);

        return $query->update($data);
    }

    /**
     * Increments records fields by filter in the database.
     *
     * @param array $filters
     * @param array $increments
     * @return void
     */
    public function increment(array $filters, array $increments): void
    {
        $query = $this->model->newQuery();

        $this->applyDynamicFilters($query, $filters);

        $query->incrementEach($increments);
    }

    /**
     * Decrements records fields by filter in the database.
     *
     * @param array $filters
     * @param array $increments
     * @return void
     */
    public function decrement(array $filters, array $increments): void
    {
        $query = $this->model->newQuery();

        $this->applyDynamicFilters($query, $filters);

        $query->decrementEach($increments);
    }

    /**
     * Delete a record from the database.
     *
     * @param mixed $id
     * @return void
     */
    public function delete($id): void
    {
        $this->model->where('id', $id)->delete();
    }

    /**
     * Delete a record from the database by the given filters.
     *
     * @param array $data
     * @return void
     */
    public function deleteBy(array $data): int
    {
        $query = $this->model->newQuery();

        $this->applyDynamicFilters($query, $data);

        return $query->delete();
    }

    /**
     * Get a record by its ID.
     *
     * @param mixed $id
     * @param array $with
     * @param array $columns
     * @return Model
     */
    public function getByIdOrFail($id, array $with = [], array $columns = ['*']): Model
    {
        return $this->model->with($with)->findOrFail($id, $columns);
    }

    /**
     * Get One record with optional filters.
     *
     * @param array $filters
     * @param array $with
     * @param array $columns
     * @return Model|null
     */
    public function getOneBy(array $filters = [], array $with = [], array $columns = ['*'])
    {
        return $this->getSearchQuery($filters, $with, [], $columns)->first();
    }

    /**
     * Get One record with optional filters or fail.
     *
     * @param array $filters
     * @param array $with
     * @param array $columns
     * @return Model|null
     */
    public function getOneByOrFail(array $filters = [], array $with = [], array $columns = ['*'])
    {
        return $this->getSearchQuery($filters, $with, [], $columns)->firstOrFail();
    }

    /**
     * Get all records with optional filters.
     *
     * @param array $filters
     * @param array $with
     * @param array $sortList
     * @param array $columns
     * @return Collection
     */
    public function getAll(array $filters = [], array $with = [], array $sortList = [], array $columns = ['*'])
    {
        $query = $this->getSearchQuery($filters, $with, $sortList, $columns);

        return $query->get();
    }

    /**
     * @param array $filters
     * @param Expression|string $columns
     * @return int
     */
    public function count(array $filters = [], Expression|string $columns = '*')
    {
        return $this->getSearchQuery($filters)
            ->count($columns);
    }

    /**
     * Get chunked records with optional filters.
     *
     * @param int $count
     * @param callable $callback
     * @param array $filters
     * @param array $with
     * @param array $sortList
     * @param array $columns
     * @return bool
     */
    public function getChunks(int $count, callable $callback, array $filters = [], array $with = [], array $sortList = [], array $columns = ['*'])
    {
        $query = $this->getSearchQuery($filters, $with, $sortList, $columns);

        return $query->chunk($count, $callback);
    }

    /**
     * Check if a record exists with the given filters.
     *
     * @param array $filters
     * @return bool
     */
    public function exists(array $filters = []): bool
    {
        $query = $this->model->newQuery();

        $this->applyDynamicFilters($query, $filters);

        return $query->exists();
    }

    /**
     * Search for records with optional filters and pagination.
     *
     * @param array $filters
     * @param array $pagination
     * @param array $with
     * @param array $columns
     * @return LengthAwarePaginator
     */
    public function search(array $filters = [], array $pagination = [], array $with = [], array $columns = ['*']): LengthAwarePaginator
    {
        $query = $this->getSearchQuery($filters, $with, $pagination['sort'] ?? [], $columns);

        return $query->paginate(
            $pagination['per_page'] ?? 20,
            ['*'],
            'page',
            $pagination['page'] ?? 1
        );
    }

    /**
     * Apply dynamic filters to the query.
     *
     * @param Builder $query
     * @param array $filters
     * @return void
     */
    protected function applyDynamicFilters(Builder $query, array $filters): void
    {
        foreach ($filters as $column => $value) {
            if ($value !== null) {
                if (is_array($value)) {
                    if (str_ends_with($column, '_not')) {
                        $query->whereNotIn(str_replace('_not', '', $column), $value);
                    } else {
                        $query->whereIn($column, $value);
                    }
                } else {
                    if (str_ends_with($column, '_starts')) {
                        $query->where(str_replace('_starts', '', $column), 'like', "{$value}%");
                    } else if (str_ends_with($column, '_not')) {
                        $query->whereNot(str_replace('_not', '', $column), $value);
                    } else {
                        $query->where($column, $value);
                    }
                }
            }
        }
    }

    /**
     * @param array $filters
     * @param array $with
     * @param array $sortList
     * @param array $columns
     * @return Builder
     */
    public function getSearchQuery(array $filters = [], array $with = [], array $sortList = [], array $columns = ['*']): Builder
    {
        $query = $this->model->newQuery()->with($with)->select($columns);

        $this->applyDynamicFilters($query, Arr::except($filters, ['per_page', 'page', 'sort']));

        if (isset($sortList)) {
            foreach ($sortList as $sort) {
                $sortParts = explode(',', $sort);
                $query->orderBy($sortParts[0], $sortParts[1] ?? 'asc');
            }
        }
        return $query;
    }
}
