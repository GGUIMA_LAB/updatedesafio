<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class SearchUserRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'nullable',
            'email' => 'nullable',
            'document' => 'nullable',
            'address' => 'nullable',
            'state' => 'nullable',
            'city' => 'nullable',
            'sexology' => 'nullable',
            'birthday' => 'nullable',
            'password' => 'nullable',
            'active' => 'nullable',
        ];
    }
}
