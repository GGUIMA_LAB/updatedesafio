<?php

namespace App\Http\Requests\User;

use App\Services\UserService;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'document' => 'required|string|unique:users,document',
            'address' => 'required|string',
            'state' => 'required|string',
            'city' => 'required|string',
            'sexology' => 'required|in:Male,Female,Other',
            'birthday' => 'required|date',
            'password' => ['required'],
            'active' => 'required|in:A,I,B',
        ];
    }
}
