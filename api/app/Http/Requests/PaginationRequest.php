<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaginationRequest extends FormRequest
{
    public function rules()
    {
        return [
            'page' => 'int',
            'per_page' => 'int',
            'sort' => 'array',
        ];
    }

    public function all($keys = null)
    {
        $data = parent::all($keys);

        return [
            'page' => $data['page'] ?? 1,
            'per_page' => $data['per_page'] ?? 20,
            'sort' => $data['sort'] ?? [],
        ];
    }
}
