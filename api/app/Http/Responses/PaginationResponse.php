<?php

namespace App\Http\Responses;
class PaginationResponse
{
    public $current_page;

    public $data;

    public $first_page_url;

    public $from;

    public $last_page;

    public $last_page_url;

    public $next_page_url;

    public $path;

    public $per_page;

    public $prev_page_url;

    public $to;

    public $total;
}
