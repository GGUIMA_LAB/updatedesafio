<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaginationRequest;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\SearchUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Services\UserService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param SearchUserRequest $request
     * @param PaginationRequest $pagination
     * @return JsonResponse
     */
    public function index(SearchUserRequest $request, PaginationRequest $pagination): JsonResponse
    {
        $paginator = $this->userService->search(
            $request->validated(),
            $pagination->validated(),
        );

        return response()->json($paginator);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUserRequest $request
     * @return JsonResponse
     */
    public function create(CreateUserRequest $request): JsonResponse
    {
        try {
            $userData = $request->validated();
            $user = $this->userService->create($userData);

            return response()->json($user, 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Falha ao criar o usuário.', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        try {
            $user = $this->userService->show($id);

            return response()->json($user, 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Usuário não encontrado.'], 404);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Falha ao obter o usuário.', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     * @return JsonResponse
     */
    public function update(int $id, UpdateUserRequest $request): JsonResponse
    {
        try {
            $userData = $request->validated();
            $this->userService->update($id, $userData);

            return response()->json(['message' => 'Usuário atualizado com sucesso.'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Falha ao atualizar o usuário.', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            $user = $this->userService->deactivate($id);

            return response()->json($user, 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Usuário não encontrado.'], 404);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Falha ao desativar o usuário.', 'error' => $e->getMessage()], 500);
        }
    }
}
