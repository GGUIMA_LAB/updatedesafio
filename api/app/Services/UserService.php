<?php

namespace App\Services;

use App\Models\User;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserService
{
    protected UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Search for users based on given filters and pagination parameters.
     *
     * @param array $filters The filters to apply for the search.
     * @param array $pagination The pagination parameters.
     * @return LengthAwarePaginator The paginated result of the search.
     */
    public function search(array $filters, array $pagination): LengthAwarePaginator
    {
        return $this->userRepository->search($filters, $pagination);
    }

    /**
     * Create a new user.
     *
     * @param array $data
     * @return User
     */
    public function create(array $data): User
    {
        return DB::transaction(function () use ($data) {
            return $this->userRepository->create([
                'name' => $data['name'],
                'email' => $data['email'],
                'document' => $data['document'],
                'address' => $data['address'],
                'state' => $data['state'],
                'city' => $data['city'],
                'sexology' => $data['sexology'],
                'birthday' => $data['birthday'],
                'password' => Hash::make($data['password']),
                'active' => $data['active'],
            ]);
        });
    }


    /**
     * Update a user.
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    public function update(int $id, array $data): void
    {
        DB::transaction(function () use ($id, $data) {
            $user = $this->userRepository->getByIdOrFail($id);

            if (isset($data['password'])) {
                $data['password'] = Hash::make($data['password']);
            }

            if ($user->active === User::DELETED) {
                $data['active'] = User::ACTIVE;
            }

            $this->userRepository->update($id, $data);
        });
    }

    /**
     * Retrieve a user by its ID.
     *
     * @param int $id
     * @return Model
     */
    public function show(int $id): Model
    {
        return $this->userRepository->getByIdOrFail($id);
    }

    /**
     * Deactivate a user.
     *
     * @param int $id
     * @return void
     */
    public function deactivate(int $id): void
    {
        DB::transaction(function () use ($id) {
            $this->userRepository->update($id, [
                'active' => User::DELETED,
            ]);
        });
    }
}
