<?php

namespace App\Services;

use App\Repositories\BaseRepository;
use Illuminate\Contracts\Database\Query\Expression;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class BaseService
{
    protected mixed $repository;

    /**
     * @param BaseRepository $repository
     */
    public function __construct(mixed $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get an record by ID.
     *
     * @param string $id
     * @param array $with
     * @param array $columns
     * @return mixed
     */
    public function getByIdOrFail(string $id, array $with = [], array $columns = ['*'])
    {
        return $this->repository->getByIdOrFail($id, $with, $columns);
    }

    /**
     * Get One record with optional filters.
     *
     * @param array $filters
     * @param array $with
     * @param array $columns
     * @return mixed|null
     */
    public function getOneBy(array $filters = [], array $with = [], array $columns = ['*'])
    {
        return $this->repository->getOneBy($filters, $with, $columns);
    }

    /**
     * Get One record with optional filters or fail.
     *
     * @param array $filters
     * @param array $with
     * @param array $columns
     * @return mixed|null
     */
    public function getOneByOrFail(array $filters = [], array $with = [], array $columns = ['*'])
    {
        return $this->repository->getOneByOrFail($filters, $with, $columns);
    }

    /**
     * Get all records with optional filters.
     *
     * @param array $filters
     * @param array $with
     * @param array $columns
     * @return Collection
     */
    public function getAll(array $filters = [], array $with = [], array $sortList = [], array $columns = ['*'])
    {
        return $this->repository->getAll($filters, $with, $sortList, $columns);
    }

    /**
     * @param array $filters
     * @param Expression|string $columns
     * @return int
     */
    public function count(array $filters = [], Expression|string $columns = '*')
    {
        return $this->repository->count($filters, $columns);
    }

    /**
     * Check if a record exists with the given filters.
     *
     * @param array $filters
     * @return bool
     */
    public function exists(array $filters = []): bool
    {
        return $this->repository->exists($filters);
    }

    /**
     * Search for records with optional filters and pagination.
     *
     * @param array $filters
     * @param array $pagination
     * @param array $with
     * @param array $columns
     * @return LengthAwarePaginator
     */
    public function search(array $filters = [], array $pagination = [], array $with = [], array $columns = ['*'])
    {
        return $this->repository->search($filters, $pagination, $with, $columns);
    }

    /**
     * Get chunked records with optional filters.
     *
     * @param int $count
     * @param callable $callback
     * @param array $filters
     * @param array $with
     * @param array $sortList
     * @param array $columns
     * @return bool
     */
    public function getChunks(int $count, callable $callback, array $filters = [], array $with = [], array $sortList = [], array $columns = ['*'])
    {
        return $this->repository->getChunks($count, $callback, $filters, $with, $sortList, $columns);
    }
}
