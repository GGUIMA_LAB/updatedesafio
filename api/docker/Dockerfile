FROM php:8.2-apache
WORKDIR /var/www

# Mod Rewrite
RUN a2enmod rewrite

# Linux Library
RUN apt-get update && apt-get install --no-install-recommends -y \
    libzip-dev \
    libxml2-dev \
    mariadb-client \
    zip \
    unzip \
    libpq-dev \
    libpng-dev \
    supervisor \
    && apt-get clean  \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN pecl install zip pcov
RUN docker-php-ext-enable zip \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install bcmath \
    && docker-php-ext-install soap \
    && docker-php-ext-install gd \
    && docker-php-source delete

# COMPOSER
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# CONFIGURE TIMEZONE
ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# APACHE SETTINGS
COPY etc/host.conf /etc/apache2/conf-enabled/host.conf
COPY etc/000-default-backend.conf /etc/apache2/sites-enabled/000-default.conf
COPY etc/apache2.conf /etc/apache2/apache2.conf

# PHP SETTINGS
COPY etc/production.ini /usr/local/etc/php/conf.d/production.ini
COPY /etc/php.ini /opt/docker/etc/php/php.ini

COPY entrypoint/entrypoint.sh /usr/local/bin/entrypoint.sh
COPY entrypoint/tdd.sh /usr/local/bin/
COPY sh/porcentage.sh /usr/local/bin/
COPY sh/color.sh /usr/local/bin/

COPY supervisor/laravel-scheduler.conf /etc/supervisor/conf.d/laravel-scheduler.conf

COPY ../ .

RUN chmod +x /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/tdd.sh
RUN chmod +x /usr/local/bin/porcentage.sh
RUN chmod +x /usr/local/bin/color.sh

CMD [ "sh", "-c", "/usr/local/bin/entrypoint.sh; supervisord; /usr/local/bin/apache2-foreground" ]
